<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Publisher;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;


class PublisherController extends FOSRestController
{

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @ApiDoc(
     *      statusCodes={
     *          200="Returned when successful",
     *          },
     *            description = "Get all Publishers",
     *       )
     */
    public function getPublishersAction()
    {
        $result = $this->get('app.publisher.service')->get();
        return $this->handleView($this->view($result, 200));
    }

    /**
     * @param $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @View
     * @ApiDoc(
     *      statusCodes={
     *          200="Returned when successful",
     *          400="Returned when the Publisher name is empty",
     *          },
     *            description = "Create new Publisher",
     *           requirements = {
     *                {"name"="name"}
     *            },
     *       )
     */
    public function postPublisherAction(Request $request)
    {
        $params = $request->request->all();
        $result = $this->get('app.publisher.service')->create($params);
        if (!$result instanceof Publisher) {
            return $this->handleView($this->view($result, 400));
        }
        
        return $this->handleView($view = $this->view($result, 200));
    }

}
