<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Author;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;


class AuthorController extends FOSRestController
{

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @ApiDoc(
     *      statusCodes={
     *          200="Returned when successful",
     *          },
     *            description = "Get all Authors",
     *       )
     */
    public function getAuthorsAction()
    {
        $result = $this->get('app.author.service')->get();
        return $this->handleView($this->view($result, 200));
    }
    
    /**
     * @param $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @View
     * @ApiDoc(
     *      statusCodes={
     *          200="Returned when successful",
     *          400="Returned when the Author name is empty",
     *          },
     *            description = "Create new Author",
     *           requirements = {
     *                {"name"="name", "id"="id"}
     *            },
     *       )
     */
    public function postAuthorAction(Request $request)
    {
        $params = $request->request->all();
        $result = $this->get('app.author.service')->create($params);
        if (!$result instanceof Author) {
            return $this->handleView($this->view($result, 400));
        }
        return $this->handleView($this->view($result, 200));
    }
    
}
