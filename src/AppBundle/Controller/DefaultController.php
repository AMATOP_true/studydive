<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/lists", name="lists")
     */
    public function listsAction()
    {
        $lists = $this->get('app.book.service')->get();
        return $this->render("default/index.html.twig", ['lists' => $lists]);
    }
}
