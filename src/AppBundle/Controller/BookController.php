<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Book;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;


class BookController extends FOSRestController
{
    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     * @View(serializerGroups={"books"})
     * @Rest\Get("/books/{id}", defaults={"id"=null})
     * @ApiDoc(
     *      statusCodes={
     *          200="Returned when successful",
     *          404="Book not found"
     *          },
     *            description = "Get all Books",
     *       )
     */
    public function getBooksAction($id)
    {
        $result = $this->get('app.book.service')->get($id);
        return $this->handleView($this->view($result, 200));
    }

    /**
     * @param $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @View(serializerGroups={"books"})
     * @ApiDoc(
     *      statusCodes={
     *          200="Returned when successful",
     *          400="Author, publisher, or title is empty"
     *          },
     *            description = "Create new Book",
     *            requirements = {
     *                {"name"="author", "datatype"="integer", "description" = "Author id" },
     *                {"name"="publisher", "datatype"="integer", "description" = "Publisher id" },
     *                {"name"="title", "type"="string", "description" = "Title for book" }
     *          },
     *       )
     */
    public function postBooksAction(Request $request)
    {
        $params = $request->request->all();
        $result = $this->get('app.book.service')->create($params);
        if (!$result instanceof Book) {
            return $this->handleView($this->view($result, 400));
        }

        return $this->handleView($view = $this->view($result, 200));

    }

    /**
     * @param $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @View(serializerGroups={"books"})
     * @ApiDoc(
     *      statusCodes={
     *          200="Returned when successful",
     *          400="Author, publisher, or title is empty",
     *          404="Book is not found"
     *          },
     *            description = "Update Book",
     *            requirements = {
     *                {"name"="id", "datatype"="integer", "description" = "Book id for update" },
     *                {"name"="author", "datatype"="integer", "description" = "Author id" },
     *                {"name"="publisher", "datatype"="integer", "description" = "Publisher id" },
     *                {"name"="title", "type"="string", "description" = "Title for book" }
     *          },
     *       )
     */
    public function putBooksAction(Request $request)
    {
        $params = $request->request->all();
        $result = $this->get('app.book.service')->update($params);
        if (!$result instanceof Book) {
            return $this->handleView($this->view($result, 400));
        }
        return $this->handleView($view = $this->view($result, 200));
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     * @View(serializerGroups={"books"})
     * @ApiDoc(
     *      statusCodes={
     *          200="Returned when successful",
     *          404="Book is not found"
     *          },
     *            description = "Delete Book",
     *            requirements = {
     *                {"name"="id", "datatype"="integer", "description" = "Book id for delete" }
     *          },
     *       )
     */
    public function deleteBooksAction($id)
    {
        $this->get('app.book.service')->delete($id);
        return $this->handleView($this->view(array('success' => true), 200));
    }

}
