<?php

namespace AppBundle\Service;


use Doctrine\ORM\EntityManager;
use AppBundle\Entity\Author;
use Symfony\Component\Validator\Validator\ValidatorInterface;


class AuthorService
{

    private $em;

    private $validator;


    public function __construct(EntityManager $em, ValidatorInterface $validator)
    {
        $this->em = $em;
        $this->validator = $validator;
    }

    public function get()
    {
        return $author = $this->em->getRepository(Author::class)->findAll();
    }

    public function create($params)
    {
        $author = new Author();
        $name = $params['name'];
        $author->setName($name);
        $errors = $this->validator->validate($author);

        if (count($errors) > 0) {

            return $errors;
        }
        $this->em->persist($author);
        $this->em->flush();

        return $author;
    }

}