<?php

namespace AppBundle\Service;


use Doctrine\ORM\EntityManager;
use AppBundle\Entity\Author;
use AppBundle\Entity\Publisher;
use AppBundle\Entity\Book;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


class BookService
{

    protected $em;

    private $validator;

    public function __construct(EntityManager $em, ValidatorInterface $validator)
    {
        $this->em = $em;
        $this->validator = $validator;
    }

    public function get($id = null)
    {
        if ($id !== null) {
            $book = $this->em->getRepository(Book::class)->find($id);

            if (!$book instanceof Book) {
                throw new NotFoundHttpException('Book not found');
            }

            return $book;
        }
        return $books = $this->em->getRepository(Book::class)->findAll(); // todo findAll() ??
    }

    public function create($params)
    {
        $book = new Book();
        $author = $this->em->getRepository(Author::class)->find($params['author']);
        $publisher = $this->em->getRepository(Publisher::class)->find($params['publisher']);
        $book->setAuthor($author);
        $book->setPublisher($publisher);
        $book->setTitle($params['title']);

        $errors = $this->validator->validate($book);

        if (count($errors) > 0) {

            return $errors;
        }
        $this->em->persist($book);
        $this->em->flush();

        return $book;
    }

    public function update($params)
    {
        $book = $this->em->getRepository(Book::class)->find($params['id']);
        if (!$book instanceof Book) {
            throw new NotFoundHttpException('Book not found');
        }
        $author = $this->em->getRepository(Author::class)->find($params['author']);
        $publisher = $this->em->getRepository(Publisher::class)->find($params['publisher']);
        $book->setAuthor($author);
        $book->setPublisher($publisher);
        $book->setTitle($params['title']);
        $validator = $this->validator;
        $errors = $validator->validate($book);

        if (count($errors) > 0) {

            return $errors;
        }
        $this->em->flush();

        return $book;
    }

    public function delete($id)
    {
        $book = $this->em->getRepository(Book::class)->find($id);
        if (!$book instanceof Book) {
            throw new NotFoundHttpException('Book not found');
        }
        $this->em->remove($book);
        $this->em->flush();
        return $book;
    }

}