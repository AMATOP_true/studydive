<?php

namespace AppBundle\Service;


use Doctrine\ORM\EntityManager;
use AppBundle\Entity\Publisher;
use Symfony\Component\Validator\Validator\ValidatorInterface;


class PublisherService
{

    private $em;

    private $validator;


    public function __construct(EntityManager $em, ValidatorInterface $validator)
    {
        $this->em = $em;
        $this->validator = $validator;
    }

    public function get()
    {
        return $author = $this->em->getRepository(Publisher::class)->findAll();
    }

    public function create($params)
    {
        $publisher = new Publisher();
        $name = $params['name'];
        $publisher->setName($name);
        $errors = $this->validator->validate($publisher);
        if (count($errors) > 0) {

            return $errors;
        }
        $this->em->persist($publisher);
        $this->em->flush();

        return $publisher;
    }
    
}